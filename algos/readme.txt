This directory contains algorithms/functions for mostly used concepts/programs.

The extension for algo files is choosen as .c|.cpp|.java etc so that the editor can display the content in color format for better readability.



Every file has below format:

/*****************************
   Concept/Description/Trick
*****************************/

Pseudo Code

/****************************
  Time/Space Complexity
****************************/
